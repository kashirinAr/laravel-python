@extends('layouts.app')
<style>
    .left-card .card-header, .right-card .card-header {
        background: #504d4d;
        color: white;
    }
    .left-card .card-body {
        color: white;
        min-height: 150px;
        background: black;
    }
    .right-card .card-body {
        min-height: 220px;
        color: white;
        background: #dcd1d1;
    }
</style>
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="row col-md-12">
            <div class="col-md-6">
                <div class="card left-card">
                    <div class="card-header">CODER TIPS</div>

                    <div class="card-body">
                    </div>
                </div>
                <div class="card left-card">
                    <div class="card-header">CODER</div>
                    <div class="card-body col-md-12">
                        <div class="float-left" style="width:80%;">
                            <textarea name="query" id="query"></textarea>
                        </div>
                        <div class="float-left">
                            <button id="query_send" class="btn btn-success">Run</button>
                            <button id="query_clear" class="btn btn-warning">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card left-card col-md-8">
                        <div class="card-header">COMPILER</div>
    
                        <div class="card-body">
                            <div>
                                <p id="compile_result"></p>
                            </div>
                           
                        </div>
                    </div>
                    <div class="card left-card col-md-4">
                        <div class="card-header">VARIABLES</div>

                        <div class="card-body">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card right-card">
                    <div class="card-header">CONTROL ROOM</div>

                    <div class="card-body">
                        
                    </div>
                    <div class="card-footer row col-md-12">
                        <input class="form-control col-md-10" id="">
                        <button class="btn btn-primary col-md-2">Send</button>
                    </div>
                </div>
                <div class="card right-card">
                    <div class="card-header">FIELD</div>

                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$( document ).ready(function() {
    var te_query = document.getElementById("query");
    var editor_python = CodeMirror.fromTextArea(te_query, {
        lineNumbers: true,
        mode: "python",
        keyMap: "sublime",
        autoCloseBrackets: true,
        matchBrackets: true,
        showCursorWhenSelecting: true,
        theme: "monokai",
        tabSize: 2
  });

  $("#query_send").on("click", function(){
    var text_query = editor_python.getValue()
    $.ajax({
        type: "POST",
        url: "{{route('compileResult')}}",
        // The key needs to match your method's input parameter (case-sensitive).
        data: ({ 'text_query': text_query, "_token": $('meta[name="csrf-token"]').attr('content') }),
        success: function(data)
        {
            $("#compile_result").html("the result is "+data)
        },
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
  });
});
</script>

@endsection